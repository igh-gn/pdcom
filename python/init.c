/*****************************************************************************
 *
 * $Id$
 *
 * Copyright (C) 2015-2020  Richard Hacker (lerichi at gmx dot net)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include "config.h"
extern const char* pdcom_version_string;

PyObject *ProcessType_Object(void);
PyObject *VariableType_Object(void);
PyObject *SubscriberType_Object(void);

/* List of functions defined in the module */

PyDoc_STRVAR(module_doc,
        "pdcom python interface");

    static int
pdcom_modexec(PyObject *m)
{
    PyObject *o;

    /* Add version tuple */
    o = PyTuple_New(2);
    if (!o)
        goto fail;
    PyTuple_SET_ITEM(o, 0, PyUnicode_FromString(pdcom_version_string));
    PyTuple_SET_ITEM(o, 1, PyUnicode_FromString(QUOTE(PYVERSION)));
    PyObject_SetAttrString(m, "version", o);
    Py_DECREF(o);

    /* Add Process */
    o = ProcessType_Object();
    if (o == NULL)
        goto fail;
    PyModule_AddObject(m, "Process", o);

    /* Add Variable */
    o = VariableType_Object();
    if (o == NULL)
        goto fail;
    PyModule_AddObject(m, "Variable", o);

    /* Add Variable */
    o = SubscriberType_Object();
    if (o == NULL)
        goto fail;
    PyModule_AddObject(m, "Subscriber", o);

    return 0;

fail:
    Py_XDECREF(m);
    return -1;
}

static PyModuleDef_Slot pdcom_slots[] = {
    {Py_mod_exec, pdcom_modexec},
    {0, NULL}
};

static struct PyModuleDef pdcommodule = {
    PyModuleDef_HEAD_INIT,
    "pdcom",
    module_doc,
    0,
    0,
    pdcom_slots,
    NULL,
    NULL,
    NULL
};

/* Export function for the module (*must* be called PyInit_pdcom) */

    PyMODINIT_FUNC
PyInit_pdcom(void)
{
    return PyModuleDef_Init(&pdcommodule);
}
