/*****************************************************************************
 *
 * $Id$
 *
 * Copyright (C) 2015-2020  Richard Hacker (lerichi at gmx dot net)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#define PY_SSIZE_T_CLEAN
//#define Py_LIMITED_API
#include <Python.h>
#include <structmember.h>   // struct PyMemberDef

#include "../pdcom4/Process.h"
#include "../pdcom4/Variable.h"

#include <stdexcept>
#include <list>
#include <map>
#include <set>

#define OBJ(self) ((PyObject*)self)
#define SELF(o)   ((ProcessObject*)o)
#define PDCOM(o) (((ProcessObject*)o)->pdcom)

class Process;
PyObject *Variable_New(PyObject *process, const PdCom::Variable *var);

PdCom::Subscriber *Subscriber_increment(PyObject*, PdCom::Process*);
PdCom::Subscriber *Subscriber_monitor(PyObject*, PdCom::Process*, bool);
PyObject *Subscription_New(
        PyObject* /*subscriber_ref*/, PyObject* /*process_ref*/,
        const PdCom::Variable::Subscription *);

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
typedef struct {
    PyObject_HEAD //;
    Process* pdcom;
    PyObject *read;
    PyObject *write;
    PyObject *flush;
    PyObject *connected;
} ProcessObject;


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
struct Process: PdCom::Process
{/*{{{*/
    ProcessObject* const self;
    std::map<const PdCom::Variable*, PyObject* /*var*/> variableMap;

    typedef std::map<const PdCom::Variable::Subscription*, PyObject*>
        SubscriptionMap;
    SubscriptionMap subscriptionMap;

    Process(PyObject *o): self(SELF(o)) {}

    ////////////////////////////////////////////////////////////
    PyObject* getVariableObject(const PdCom::Variable *v)
    {/*{{{*/
        PyObject*& obj = variableMap[v];

//        printf("%s %p\n", __func__, obj);
        if (obj)
            Py_INCREF(obj);
        else
            obj = Variable_New(OBJ(self), v);

        return obj;
    }/*}}}*/

    ////////////////////////////////////////////////////////////
    static PyObject *makeMessage(const Message& m)
    {/*{{{*/
        PyObject *o = PyTuple_New(6);
        if (o) {
            PyTuple_SET_ITEM(o, 0, PyLong_FromUnsignedLong(m.seqNo));
            PyTuple_SET_ITEM(o, 1, PyLong_FromLong(m.level));
            PyTuple_SET_ITEM(o, 2, PyUnicode_FromString(m.path.c_str()));
            PyTuple_SET_ITEM(o, 3, PyLong_FromLong(m.index));
            PyTuple_SET_ITEM(o, 4, PyLong_FromUnsignedLongLong(m.time_ns));
            PyTuple_SET_ITEM(o, 5, PyUnicode_FromString(m.text.c_str()));
        }
        return o;
    }/*}}}*/

    ////////////////////////////////////////////////////////////
    // Reimplemented from PdCom::Process
    ////////////////////////////////////////////////////////////
    int read(       char *buf, size_t _buf_size)
    {/*{{{*/
        PyObject *buf_size = PyLong_FromSize_t(_buf_size);
        if (!buf_size)
            return 0;

        PyObject *obj = PyObject_CallFunctionObjArgs(
                self->read, OBJ(self), buf_size, NULL);
        Py_DECREF(buf_size);
        if (!obj)       // an exception occurred
            return 0;

        size_t len = 0;
        const char* bytes = PyBytes_AsString(obj);
        if (bytes) {
            len = PyObject_Length(obj);
            if (buf_size - len <= buf_size) {
                std::copy(bytes, bytes + len, buf);
            } else {
                len = 0;
                PyErr_SetString(PyExc_AttributeError, "Buffer too long");
            }
        }
        Py_DECREF(obj);

        return len;
    }/*}}}*/

    void write(const char *buf, size_t count)
    {/*{{{*/
        PyObject *obj = PyBytes_FromStringAndSize(buf, count);
        if (!obj)
            return;

        Py_XDECREF(PyObject_CallFunctionObjArgs(
                    self->write, OBJ(self), obj, NULL));
        Py_DECREF(obj);
    }/*}}}*/

    void flush()
    {/*{{{*/
        Py_XDECREF(PyObject_CallFunctionObjArgs(
                    self->flush, OBJ(self), NULL));
    }/*}}}*/

    void connected()
    {/*{{{*/
        Py_XDECREF(PyObject_CallFunctionObjArgs(
                    self->connected, OBJ(self), NULL));
    }/*}}}*/

    std::string applicationName() const
    {/*{{{*/
        PyObject *callable =
            PyObject_GetAttrString(OBJ(self), "applicationName");
        if (!callable) {
            PyErr_Clear();
            return PdCom::Process::applicationName();
        }

        std::string str;

        PyObject *value = PyObject_CallFunctionObjArgs(callable, NULL);
        if (value) {
            Py_ssize_t count;
            const char *bytes = PyUnicode_AsUTF8AndSize(value, &count);
            if (bytes)
                str.assign(bytes, count);

            Py_DECREF(value);
        }
        Py_DECREF(callable);

        return str;
    }/*}}}*/

    std::string hostname() const
    {/*{{{*/
        PyObject *callable = PyObject_GetAttrString(OBJ(self), "hostname");
        if (!callable) {
            PyErr_Clear();
            return PdCom::Process::hostname();
        }

        std::string str;

        PyObject *value =
            PyObject_CallFunctionObjArgs(callable, NULL);
        if (value) {
            Py_ssize_t count;
            const char *bytes = PyUnicode_AsUTF8AndSize(value, &count);
            if (bytes)
                str.assign(bytes, count);

            Py_DECREF(value);
        }
        Py_DECREF(callable);

        return str;
    }/*}}}*/

    void listReply(
            std::list<const PdCom::Variable*>& _variables,
            std::list<std::string>& _directories)
    {/*{{{*/
        PyObject *callable = PyObject_GetAttrString(OBJ(self), "listReply");
        if (!callable)
            return;

        PyObject *variables   = PyTuple_New(  _variables.size());
        PyObject *directories = PyTuple_New(_directories.size());
        Py_ssize_t idx;

        if (variables and directories) {
            idx = 0;
            for (std::list<const PdCom::Variable*>::iterator it
                    = _variables.begin(); it != _variables.end(); ++it)
                PyTuple_SET_ITEM(variables, idx++, getVariableObject(*it));

            idx = 0;
            for (std::list<std::string>::iterator it = _directories.begin();
                    it != _directories.end(); ++it)
                PyTuple_SET_ITEM(directories, idx++,
                        PyUnicode_FromString(it->c_str()));

            Py_XDECREF(PyObject_CallFunctionObjArgs(callable,
                        variables, directories, NULL));
        }

        Py_XDECREF(variables);
        Py_XDECREF(directories);
        Py_DECREF(callable);
    }/*}}}*/

    void findReply(const PdCom::Variable* var)
    {/*{{{*/
        PyObject *callable = PyObject_GetAttrString(OBJ(self), "findReply");
        if (!callable)
            return;

        PyObject *o;
        if (var) {
            o = getVariableObject(var);
        } else {
            o = Py_None;
            Py_INCREF(o);
        }

        Py_XDECREF(PyObject_CallFunctionObjArgs(callable, o, NULL));
        Py_DECREF(o);
        Py_DECREF(callable);
    }/*}}}*/

    void loginReply(const char* _mechlist, const char* _data, int _finished)
    {/*{{{*/
        PyObject *callable = PyObject_GetAttrString(OBJ(self), "loginReply");
        if (!callable)
            return;

        PyObject *mechlist = PyBytes_FromString(_mechlist);
        PyObject *data = PyBytes_FromString(_data);
        PyObject *finished = PyLong_FromLong(_finished);

        Py_XDECREF(PyObject_CallFunctionObjArgs(callable,
                    mechlist, data, finished, NULL));
        Py_XDECREF(mechlist);
        Py_XDECREF(data);
        Py_XDECREF(finished);
        Py_DECREF(callable);
    }/*}}}*/

    void pingReply()
    {/*{{{*/
        PyObject *callable = PyObject_GetAttrString(OBJ(self), "pingReply");
        if (!callable)
            return;

        Py_XDECREF(PyObject_CallFunctionObjArgs(callable, NULL));
        Py_DECREF(callable);
    }/*}}}*/

    void startTLSReply()
    {/*{{{*/
        PyObject *callable = PyObject_GetAttrString(OBJ(self), "startTLSReply");
        if (!callable)
            return;

        Py_XDECREF(PyObject_CallFunctionObjArgs(callable, NULL));
        Py_DECREF(callable);
    }/*}}}*/

    bool alive()
    {/*{{{*/
        PyObject *callable = PyObject_GetAttrString(OBJ(self), "alive");
        if (!callable) {
            PyErr_Clear();
            return true;
        }

        PyObject* o = PyObject_CallFunctionObjArgs(callable, NULL);
        bool rv = o and PyObject_IsTrue(o);
        Py_DECREF(callable);
        Py_XDECREF(o);
        
        return rv;
    }/*}}}*/

    void processMessage(const Message& m)
    {/*{{{*/
        PyObject *callable =
            PyObject_GetAttrString(OBJ(self), "processMessage");
        if (!callable) {
            PyErr_Clear();
            return;
        }

        PyObject *message = makeMessage(m);

        if (message) {
            Py_XDECREF(PyObject_CallObject(callable, message));
            Py_DECREF(message);
        }

        Py_DECREF(callable);
    }/*}}}*/

    void getMessageReply(const Message& m)
    {/*{{{*/
        PyObject *callable =
            PyObject_GetAttrString(OBJ(self), "getMessageReply");
        if (!callable)
            return;

        PyObject *message = makeMessage(m);

        if (message) {
            Py_XDECREF(PyObject_CallObject(callable, message));
            Py_DECREF(message);
        }

        Py_DECREF(callable);
    }/*}}}*/

    void activeMessagesReply(const std::list<Message>& messages)
    {/*{{{*/
        PyObject *callable =
            PyObject_GetAttrString(OBJ(self), "activeMessagesReply");
        if (!callable)
            return;

        PyObject *list = PyTuple_New(messages.size());
        size_t idx = 0;
        for (std::list<Message>::const_iterator it = messages.begin();
                it != messages.end(); ++it)
            PyTuple_SET_ITEM(list, idx++, makeMessage(*it));

        Py_XDECREF(PyObject_CallFunctionObjArgs(callable, list, NULL));

        Py_XDECREF(list);

        Py_DECREF(callable);
    }/*}}}*/
    
    void protocolLog(LogLevel_t _level, const std::string& _message)
    {/*{{{*/
        PyObject *callable =
            PyObject_GetAttrString(OBJ(self), "protocolLog");
        if (!callable) {
            PyErr_Clear();
            return;
        }

        PyObject *level = PyLong_FromLong(_level);
        PyObject *message = PyUnicode_FromString(_message.c_str());
        Py_XDECREF(PyObject_CallFunctionObjArgs(callable,
                    level, message, NULL));

        Py_XDECREF(level);
        Py_XDECREF(message);

        Py_DECREF(callable);
    }/*}}}*/
    
    void transmitSemaphore(bool state)
    {/*{{{*/
        PyObject *callable =
            PyObject_GetAttrString(OBJ(self), "transmitSemaphore");
        if (!callable) {
            PyErr_Clear();
            return;
        }

        Py_DECREF(PyObject_CallFunctionObjArgs(callable,
                    state ? Py_True : Py_False, NULL));
        Py_DECREF(callable);
    }/*}}}*/
};/*}}}*/

////////////////////////////////////////////////////////////////////////////
// Interface functions
////////////////////////////////////////////////////////////////////////////
    PyObject *
Process_getSubscription(
        const PdCom::Variable::Subscription *subscription,
        PyObject *subscriber)
{
    Process *p = static_cast<Process*>(subscription->process);
    PyObject *&s = p->subscriptionMap[subscription];
    if (s)
        Py_INCREF(s);
    else
        s = Subscription_New(subscriber, OBJ(p->self), subscription);

    return s;
}

////////////////////////////////////////////////////////////////////////////
    void
Process_unsubscribe(PdCom::Process* p, const PdCom::Variable::Subscription* s)
{
    static_cast<Process*>(p)->subscriptionMap.erase(s);
    s->cancel();
}

////////////////////////////////////////////////////////////////////////////
    PyObject *
Process_getVariableObject(PdCom::Process *p, const PdCom::Variable *v)
{
    return static_cast<Process*>(p)->getVariableObject(v);
}

////////////////////////////////////////////////////////////////////////////
    void
Process_forgetVariable(PyObject *o, const PdCom::Variable *var)
{
//    printf("%s(%p) %p\n", __func__, o, var);
    PDCOM(o)->variableMap.erase(var);
}

////////////////////////////////////////////////////////////////////////////
    PyObject *
Process_object(PdCom::Process *p)
{
    return OBJ(static_cast<Process*>(p)->self);
}

////////////////////////////////////////////////////////////////////////////
// PyObject methods
////////////////////////////////////////////////////////////////////////////
    static PyObject *
process_reset(ProcessObject* o)
{
    PDCOM(o)->reset();
    Py_RETURN_NONE;
}

////////////////////////////////////////////////////////////////////////////
    static PyObject *
process_asyncData(ProcessObject* o)
{
    return PyLong_FromLong(PDCOM(o)->asyncData());
}

////////////////////////////////////////////////////////////////////////////
    static PyObject *
process_name(ProcessObject* o)
{
    return PyUnicode_FromString(PDCOM(o)->name().c_str());
}

////////////////////////////////////////////////////////////////////////////
    static PyObject *
process_version(ProcessObject* o)
{
    return PyUnicode_FromString(PDCOM(o)->version().c_str());
}

////////////////////////////////////////////////////////////////////////////
    static PyObject *
process_list(ProcessObject* o, PyObject* args)
{
    PyObject *result = 0;

    PyObject *path_obj = 0;
    if (!PyArg_ParseTuple(args, "|O", &path_obj))
        return NULL;

    const char *path = 0;
    if (path_obj and path_obj != Py_None) {
        path = PyUnicode_AsUTF8(path_obj);
        if (!path)
            goto fail;
    }
    result = PDCOM(o)->list(path ? std::string(path) : std::string())
        ? Py_True : Py_False;
    Py_INCREF(result);

fail:
    Py_XDECREF(path_obj);
    return result;
}

////////////////////////////////////////////////////////////////////////////
    static PyObject *
process_subscribe(ProcessObject* o, PyObject* args)
{
    PyObject *subscriber;
    const char *path;
    double interval;
    int id = -1;
    if (!PyArg_ParseTuple(args, "Osd|i", &subscriber, &path, &interval, &id))
        return NULL;

    PdCom::Subscriber *s = Subscriber_increment(subscriber, PDCOM(o));
    if (!s)
        return NULL;

    PDCOM(o)->subscribe(s, path, interval, id);

    Py_RETURN_NONE;
}

////////////////////////////////////////////////////////////////////////////
    static PyObject *
process_find(ProcessObject* o, PyObject* args)
{
    const char *path;
    if (!PyArg_ParseTuple(args, "s", &path))
        return NULL;

    PyObject *rv = PDCOM(o)->find(path) ? Py_True : Py_False;
    Py_INCREF(rv);

    return rv;
}

////////////////////////////////////////////////////////////////////////////
    static PyObject *
process_login(ProcessObject* o, PyObject* args)
{
    const char *data;
    Py_ssize_t len;
    if (!PyArg_ParseTuple(args, "y#", &data, &len))
        return NULL;

    // FIXME
    PyObject *rv = PDCOM(o)->login(0, data) ? Py_True : Py_False;
    Py_INCREF(rv);

    return rv;
}

////////////////////////////////////////////////////////////////////////////
    static PyObject *
process_logout(ProcessObject* o)
{
    PDCOM(o)->logout();
    Py_RETURN_NONE;
}

////////////////////////////////////////////////////////////////////////////
    static PyObject *
process_ping(ProcessObject* o)
{
    PDCOM(o)->ping();
    Py_RETURN_NONE;
}

////////////////////////////////////////////////////////////////////////////
    static PyObject *
process_startTLS(ProcessObject* o)
{
    PDCOM(o)->startTLS();
    Py_RETURN_NONE;
}

////////////////////////////////////////////////////////////////////////////
    static PyObject *
process_getMessage(ProcessObject* o, PyObject* args)
{
    unsigned long id;
    if (!PyArg_ParseTuple(args, "k", &id))
        return NULL;

    PDCOM(o)->getMessage(id);
    Py_RETURN_NONE;
}

////////////////////////////////////////////////////////////////////////////
    static PyObject *
process_activeMessages(ProcessObject* o)
{
    PDCOM(o)->activeMessages();
    Py_RETURN_NONE;
}

////////////////////////////////////////////////////////////////////////////
    static PyObject *
process_parameterMonitor(ProcessObject* o, PyObject* args)
{
    PyObject *subscriber;
    int state;
    if (!PyArg_ParseTuple(args, "Op", &subscriber, &state))
        return NULL;

    PdCom::Subscriber *s = Subscriber_monitor(subscriber, PDCOM(o), state);
    if (!s)
        return NULL;

    PDCOM(o)->parameterMonitor(s, state);

    Py_RETURN_NONE;
}

////////////////////////////////////////////////////////////////////////////
static PyMethodDef tp_methods[] = {
    {"reset", (PyCFunction)process_reset, METH_NOARGS,
        PyDoc_STR("reset() --> None\n\n"
                "Reset and clear connection, ready for a new connection")},
    {"asyncData", (PyCFunction)process_asyncData, METH_NOARGS,
        PyDoc_STR("asyncData() --> int\n\n"
                "Initiate a call to read() and process incoming data,\n"
                "possibly calling one of the many registered callbacks.\n"
                "\nReturns number of bytes read")},
    {"name", (PyCFunction)process_name, METH_NOARGS,
        PyDoc_STR("name() --> str\n\n"
                "Returns name of process")},
    {"version", (PyCFunction)process_version, METH_NOARGS,
        PyDoc_STR("version() --> str\n\n"
                "Returns version of process")},
    {"list", (PyCFunction)process_list, METH_VARARGS,
        PyDoc_STR("list(path:str) --> None\n\n"
                "List path, returning values in "
                "listReply(variables:list(pdcom.Variable),\n"
                "          directories:list(str)).\n"
                "If path is None or empty (\"\"), "
                "list all available variables at once")},
    {"subscribe", (PyCFunction)process_subscribe, METH_VARARGS,
        PyDoc_STR("subscribe(s:pdcom.Subscriber,\n"
                "          path:str, interval:float, tag:int) --> None\n\n"
                "Subscribe to a variable")},
    {"find", (PyCFunction)process_find, METH_VARARGS,
        PyDoc_STR("find(path:str) --> bool\n\n"
                "Search for a variable, return result in findReply(var)\n"
                "\nReturns true if findReply() was called within this call\n"
                "when the variable is cached")},
    {"login", (PyCFunction)process_login, METH_VARARGS,
        PyDoc_STR("login(mechanism,data) --> boolean\n\n"
                "Call login")},
    {"logout", (PyCFunction)process_logout, METH_NOARGS,
        PyDoc_STR("logout() --> None\n\n"
                "Call loginout")},
    {"ping", (PyCFunction)process_ping, METH_NOARGS,
        PyDoc_STR("ping() --> None\n\n"
                "ping server, replying in pingReply()")},
    {"startTLS", (PyCFunction)process_startTLS, METH_NOARGS,
        PyDoc_STR("startTLS() --> None\n\n"
                "Start of TLS. "
                "When server is ready, startTLSReply() is called")},
    {"getMessage", (PyCFunction)process_getMessage, METH_VARARGS,
        PyDoc_STR("getMessage(id:int) --> None\n\n"
                "Get a specific message. Reply comes in getMessageReply()\n"
                "The arguments are documented in processMessage()")},
    {"activeMessages", (PyCFunction)process_activeMessages, METH_NOARGS,
        PyDoc_STR("activeMessages() --> None\n\n"
                "Request all active messages.\n"
                "Reply comes in activeMessagesReply(list)\n"
                "The list arguments are documented in processMessage()")},
    {"parameterMonitor", (PyCFunction)process_parameterMonitor, METH_NOARGS,
        PyDoc_STR("parameterMonitor(s:pdcom.Subscriber, state:bool) --> None\n\n"
                "Set subscriber to monitor all parameter changes")},
    {NULL}
};


////////////////////////////////////////////////////////////////////////////
// Slots for Type
////////////////////////////////////////////////////////////////////////////
    static PyObject *
Process_tp_new(PyTypeObject *tp, PyObject *args, PyObject *kwds)
{
    PyObject *o = PyType_GenericNew(tp, args, kwds);
    if (!o)
        goto fail;

    try {
        PDCOM(o) = new Process(o);
    } catch (const std::exception& e) {
        PyErr_SetString(PyExc_Exception, e.what());
        goto fail;
    } catch(...) {
        PyErr_SetString(PyExc_MemoryError,
                "Could not allocate Process() object");
        goto fail;
    }

//    printf("%s(%p) %p\n", __func__, o, PDCOM(o));

    return o;

fail:
    Py_XDECREF(o);
    return NULL;
}

////////////////////////////////////////////////////////////////////////////
    static int
Process_tp_init(ProcessObject* self, PyObject *args, PyObject *kwds)
{
    self->read      = PyObject_GetAttrString(OBJ(Py_TYPE(self)), "read");
    self->write     = PyObject_GetAttrString(OBJ(Py_TYPE(self)), "write");
    self->connected = PyObject_GetAttrString(OBJ(Py_TYPE(self)), "connected");
    self->flush     = PyObject_GetAttrString(OBJ(Py_TYPE(self)), "flush");

    return PyErr_Occurred() ? -1 : 0;
}

////////////////////////////////////////////////////////////////////////////
    static void
Process_tp_dealloc(ProcessObject *o)
{
//    printf("%s(%p) %p\n", __func__, o, PDCOM(o));
    delete PDCOM(o);
    Py_XDECREF(o->read);
    Py_XDECREF(o->write);
    Py_XDECREF(o->connected);
    Py_XDECREF(o->flush);

    Py_TYPE(o)->tp_free(o);
}

////////////////////////////////////////////////////////////////////////////
PyDoc_STRVAR(tp_doc,
    "Main Process object\n\n"
    "This is the main object required to interact with the server.\n"
    "\n"
    "It has a predominantly asynchronous nature. Commands are sent to the\n"
    "server by calling various methods. The methods are not blocking,\n"
    "as long as the calls to write() and flush() do not block.\n"
    "\n"
    "When data arrives on the socket, call asyncData() which in turn calls\n"
    "read() to fetch the data. During input data processing, various virual\n"
    "methods are called as required. Note that it is generally a bad idea\n"
    "to issue any commands to the server within these methods. You milage\n"
    "may vary.\n"
    "\n"
    "To use this class, the following virtual methods must be implemented:\n"
    "\n"
    "def read(self, n:int) -> bytes:\n"
    "    This method is called within the context of asyncData().\n"
    "    It should read max n bytes of data from socket and return a bytes "
    "object.\n"
    "\n"
    "def write(self, buf:bytes) -> None:\n"
    "    This method is called to send buf to the socket.\n"
    "\n"
    "def flush(self) -> int:\n"
    "    This method is called to flush data in the socket.\n"
    "    Return 0 on success, negative number on failure.\n"
    "\n"
    "def connected(self) -> None:\n"
    "    This method is called when communication with the server "
    "is established.\n"
    "    No interaction with the server is allowed before this point.\n"
    "\n"
    "The following virtual methods are optional:\n"
    "\n"
    "def applicationName() -> str\n"
    "    Return arbitrary name describing the application\n"
    "\n"
    "def alive() -> bool\n"
    "    Return true to indicate a live state (default behaviour)\n"
    "\n"
    "def processMessage(seqNo:int, level:int,\n"
    "                   path:str, idx:int, time:int, text:str) -> None\n"
    "    Called when the process generates a message\n"
    "\n"
    "There are various other methods required when calling any of the\n"
    "various server commands. See the corresponding documentation\n"
    );

static PyType_Slot tp_slots[] = {
    {Py_tp_doc, tp_doc},
    {Py_tp_methods, tp_methods},
    {Py_tp_new,         (void*)Process_tp_new},
    {Py_tp_init,        (void*)Process_tp_init},
    {Py_tp_dealloc,     (void*)Process_tp_dealloc},
    {0, 0}
};

static PyType_Spec Type_spec = {
    "pdcom.Process",
    sizeof(ProcessObject),
    0,
    Py_TPFLAGS_DEFAULT
        | Py_TPFLAGS_BASETYPE
        ,
    tp_slots
};

////////////////////////////////////////////////////////////////////////////
    extern "C" PyObject *
ProcessType_Object(void)
{
    static PyObject *type;
    if (!type) {
        type = PyType_FromSpec(&Type_spec);

        PyObject_SetAttrString(type,
                "LOG_Reset",    PyLong_FromLong(PdCom::Process::Reset));
        PyObject_SetAttrString(type,
                "LOG_Emergency", PyLong_FromLong(PdCom::Process::Emergency));
        PyObject_SetAttrString(type,
                "LOG_Alert",    PyLong_FromLong(PdCom::Process::Alert));
        PyObject_SetAttrString(type,
                "LOG_Critical", PyLong_FromLong(PdCom::Process::Critical));
        PyObject_SetAttrString(type,
                "LOG_Error",    PyLong_FromLong(PdCom::Process::Error));
        PyObject_SetAttrString(type,
                "LOG_Warn",     PyLong_FromLong(PdCom::Process::Warn));
        PyObject_SetAttrString(type,
                "LOG_Info",     PyLong_FromLong(PdCom::Process::Info));
        PyObject_SetAttrString(type,
                "LOG_Debug",    PyLong_FromLong(PdCom::Process::Debug));
        PyObject_SetAttrString(type,
                "LOG_Trace",    PyLong_FromLong(PdCom::Process::Trace));
    }
    return type;
}
