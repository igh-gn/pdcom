/*****************************************************************************
 *
 * $Id$
 *
 * Copyright (C) 2015-2020  Richard Hacker (lerichi at gmx dot net)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#define PY_SSIZE_T_CLEAN
//#define Py_LIMITED_API
#include <Python.h>

#include "../pdcom4/Subscriber.h"
#include "../pdcom4/Process.h"

#include <stdexcept>
#include <list>
#include <map>
#include <set>

#define OBJ(self) ((PyObject*)self)
#define SELF(o)   ((SubscriberObject*)o)
#define PDCOM(o) (((SubscriberObject*)o)->pdcom)

static PyObject *_type;

class Subscriber;
PyObject *Process_object(PdCom::Process *);
PyObject *Process_getSubscription(
        const PdCom::Variable::Subscription*, PyObject*);

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
typedef struct {
    PyObject_HEAD //;
    Subscriber* pdcom;
    PyObject *newGroupValue;
    PyObject *newValue;
    PyObject *active;
    PyObject *invalid;
} SubscriberObject;

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
struct Subscriber: PdCom::Subscriber
{/*{{{*/
    SubscriberObject* self;

    typedef std::map<PdCom::Process*, size_t> ProcessMap;
    ProcessMap processMap;

    typedef std::set<PdCom::Process*> ProcessSet;
    ProcessSet parameterMonitor;

    Subscriber(PyObject *o): self(SELF(o)) {
//        printf("%s(%p)\n", __func__, self);
    }
    ~Subscriber() {
//        printf("%s(%p)\n", __func__, self);
        for (ProcessMap::iterator it = processMap.begin();
                it != processMap.end(); ++it) {
            it->first->unsubscribe(this);
            Py_DECREF(Process_object(it->first));
        }

        for (ProcessSet::iterator it = parameterMonitor.begin();
                it != parameterMonitor.end(); ++it) {
            (*it)->parameterMonitor(this, false);
            Py_DECREF(Process_object(*it));
        }
    }

    ////////////////////////////////////////////////////////////
    // Reimplemented from PdCom::Subscriber
    ////////////////////////////////////////////////////////////
    void newGroupValue(uint64_t time_ns)
    {
        PyObject *obj = PyLong_FromUnsignedLongLong(time_ns);
        Py_XDECREF(PyObject_CallFunctionObjArgs(
                    self->newGroupValue, OBJ(self), obj, NULL));
        Py_XDECREF(obj);
    }

    void newValue(const PdCom::Variable::Subscription* subscription)
    {
        PyObject *o = Process_getSubscription(subscription, OBJ(self));
        Py_XDECREF(PyObject_CallFunctionObjArgs(
                    self->newValue, OBJ(self), o, NULL));
        Py_XDECREF(o);
    }

    void active(const std::string& _path,
            const PdCom::Variable::Subscription* subscription)
    {
        PyObject *path = PyUnicode_FromString(_path.c_str());
        PyObject *s = Process_getSubscription(subscription, OBJ(self));
        Py_XDECREF(PyObject_CallFunctionObjArgs(
                    self->active, OBJ(self), path, s, NULL));
        Py_XDECREF(path);
        Py_XDECREF(s);

        decrement(subscription->process);
    }

    void invalid(PdCom::Process* process, const std::string& _path, int _id)
    {
        PyObject *path = PyUnicode_FromString(_path.c_str());
        PyObject *id = PyLong_FromLong(_id);
        Py_XDECREF(PyObject_CallFunctionObjArgs(
                    self->invalid, OBJ(self),
                    Process_object(process), path, id, NULL));
        Py_XDECREF(path);
        Py_XDECREF(id);

        decrement(process);
    }

    void decrement(PdCom::Process *process)
    {
        ProcessMap::iterator it = processMap.find(process);
        if (!--it->second) {
            Py_DECREF(Process_object(it->first));
            processMap.erase(it);
        }
    }
};/*}}}*/

////////////////////////////////////////////////////////////////////////////
PdCom::Subscriber *Subscriber_increment(PyObject *o, PdCom::Process *process)
{
    if (!PyObject_IsInstance(o, _type)) {
        PyErr_SetString(PyExc_TypeError,
                "not pdcom.Subscriber type object");
        return NULL;
    }

    SubscriberObject *self = SELF(o);

    if (!PDCOM(self)->processMap[process]++)
        Py_INCREF(Process_object(process));

    return PDCOM(self);
}

////////////////////////////////////////////////////////////////////////////
PdCom::Subscriber *Subscriber_monitor(
        PyObject *o, PdCom::Process *process, bool state)
{
    if (!PyObject_IsInstance(o, _type)) {
        PyErr_SetString(PyExc_TypeError,
                "not pdcom.Subscriber type object");
        return NULL;
    }

    SubscriberObject *self = SELF(o);

    if (state) {
        if (PDCOM(self)->parameterMonitor.insert(process).second)
            Py_INCREF(Process_object(process));
    } else {
        if (PDCOM(self)->parameterMonitor.erase(process))
            Py_DECREF(Process_object(process));
    }

    return PDCOM(self);
}

////////////////////////////////////////////////////////////////////////////
// Slots for Type
////////////////////////////////////////////////////////////////////////////
PyObject * Subscriber_tp_new(PyTypeObject *tp, PyObject *args, PyObject *kwds)
{
    PyObject *o = PyType_GenericNew(tp, args, kwds);
    if (!o)
        goto fail;

    try {
        PDCOM(o) = new Subscriber(o);
    } catch (const std::exception& e) {
        PyErr_SetString(PyExc_Exception, e.what());
        goto fail;
    } catch(...) {
        PyErr_SetString(PyExc_MemoryError,
                "Could not allocate Subscriber() object");
        goto fail;
    }

//    printf("%s(%p) %p\n", __func__, o, PROCESS(o));

    return o;

fail:
    Py_XDECREF(o);
    return NULL;
}

////////////////////////////////////////////////////////////////////////////
int Subscriber_tp_init(SubscriberObject* self, PyObject *args, PyObject *kwds)
{
    self->newGroupValue =
        PyObject_GetAttrString(OBJ(Py_TYPE(self)), "newGroupValue");
    self->newValue = PyObject_GetAttrString(OBJ(Py_TYPE(self)), "newValue");
    self->active   = PyObject_GetAttrString(OBJ(Py_TYPE(self)), "active");
    self->invalid  = PyObject_GetAttrString(OBJ(Py_TYPE(self)), "invalid");

//    printf("%s(%p) %p\n", __func__, self, Py_TYPE(self));

    return PyErr_Occurred() ? -1 : 0;
}

////////////////////////////////////////////////////////////////////////////
static void Subscriber_tp_dealloc(SubscriberObject *o)
{
//    printf("%s(%p) %p\n", __func__, o, PDCOM(o));
    delete PDCOM(o);
    Py_CLEAR(o->newGroupValue);
    Py_CLEAR(o->newValue);
    Py_CLEAR(o->active);
    Py_CLEAR(o->invalid);

    Py_TYPE(o)->tp_free(o);
}

PyDoc_STRVAR(tp_doc,
    "Main Subscriber object");

static PyType_Slot tp_slots[] = {
    {Py_tp_doc, tp_doc},
    {Py_tp_new,         (void*)Subscriber_tp_new},
    {Py_tp_init,        (void*)Subscriber_tp_init},
    {Py_tp_dealloc,     (void*)Subscriber_tp_dealloc},
    {0, 0}
};

static PyType_Spec type_spec = {
    "pdcom.Subscriber",
    sizeof(SubscriberObject),
    0,
    Py_TPFLAGS_DEFAULT
        | Py_TPFLAGS_BASETYPE
        ,
    tp_slots
};

////////////////////////////////////////////////////////////////////////////
extern "C" PyObject *SubscriberType_Object(void)
{
    if (!_type)
        _type = PyType_FromSpec(&type_spec);
    return _type;
}
