#----------------------------------------------------------------------------
#
#  $Id$
#
#  Copyright (C) 2008-2016  Florian Pose <fp@igh.de>
#                           Richard Hacker (lerichi at gmx dot net)
#
#  This file is part of the PdCom library.
#
#  The PdCom library is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or (at your
#  option) any later version.
#
#  The PdCom library is distributed in the hope that it will be useful, but
#  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
#  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
#  License for more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
#
#  vim: tw=78
#----------------------------------------------------------------------------

%if 0%{?!python_module:1}
%define python_module() python-%{**}
%endif

%define pkgname @PROJECT_NAME@@MAJOR_VERSION@
%define libname lib%{pkgname}-@MAJOR_VERSION@_@MINOR_VERSION@

Name:           %{pkgname}
Version:        @MAJOR_VERSION@.@MINOR_VERSION@.@PATCH_LEVEL@
Release:        0

Summary:        Process data communication client library
License:        LGPL-3.0+
Vendor:         Ingenieurgemeinschaft IgH GmbH
Group:          System/Libraries
Url:            http://etherlab.org/en/pdcom
Source:         http://etherlab.org/download/pdcom/pdcom-%version.tar.bz2

BuildRequires:  gcc-c++ cmake doxygen fdupes
BuildRequires:  pkgconfig
BuildRequires:  pkgconfig(expat)

BuildRequires:  boost-devel

%if 0%{?suse_version} >= 1500
BuildRequires:  libboost_headers-devel
%if 0%{?sle_version}
# sle_version is undefined for tumbleweed
# See https://en.opensuse.org/openSUSE:Packaging_for_Leap#RPM_Distro_Version_Macros
BuildRequires:  libboost_python-devel
%endif
%endif

BuildRequires:  %{python_module devel}
BuildRequires:  %{python_module setuptools}

%if 0%{?py_requires:1}
%py_requires
%endif

%description
PdCom shall provide a flexible C++ API for non-realtime exchange of process
data and is part of the EtherLab project (http://etherlab.org/en/pdcom). It is
designed to be independent of any communication protocols and channels, and
shall be usable under several operating systems and platforms. I. e. it has
been tested successfully under Linux and Windows.

#----------------------------------------------------------------------------
# Build step
# This must be before packaging, because otherwise the version number
# changes
#----------------------------------------------------------------------------

# ========= PREPARE ===========
%prep
%setup -n pdcom-%{version}

# ========= BUILD   ===========
%build
%cmake --no-warn-unused-cli \
    -DCMAKE_INSTALL_SYSCONFDIR=%{_sysconfdir} \
    -DCMAKE_INSTALL_INCLUDEDIR=%{_includedir} \
    -DCMAKE_INSTALL_BINDIR=%{_bindir} \
    -DCMAKE_INSTALL_LIBDIR=%{_libdir} \
    -DPYTHONLIB=1

%if 0%{?make_jobs:1}
%make_jobs
%else
%__make %{?_smp_mflags} VERBOSE=1
%endif

# Documentation
%__make doc
%fdupes -s doc/html

# put a link to current build directory in source tree
%__ln_s -f $PWD %{_builddir}/%{buildsubdir}/_top_builddir

# ========= INSTALL ===========
%install
%if 0%{?cmake_install:1}
%cmake_install
%else
DESTDIR=%{buildroot} %__make install -C _top_builddir
%endif

#----------------------------------------------------------------------------
# C++ library
#----------------------------------------------------------------------------
%package -n %libname
Summary:        Process data communication client library
Provides:       %{name} = %{version}
Obsoletes:      %{libname} < %{version}

%description -n %libname
PdCom shall provide a flexible C++ API for non-realtime exchange of process
data and is part of the EtherLab project (http://etherlab.org/en/pdcom). It is
designed to be independent of any communication protocols and channels, and
shall be usable under several operating systems and platforms. I. e. it has
been tested successfully under Linux and Windows.

%files  -n %libname
%defattr(-,root,root,755)
%doc AUTHORS ChangeLog COPYING NEWS
%{_libdir}/lib%{pkgname}.so.*

#----------------------------------------------------------------------------
# Development package
#----------------------------------------------------------------------------
%package devel

Summary: Development files for PdCom
Group: Development/Libraries/C and C++
Requires: %{name} = %{version}
Provides: lib%{pkgname}.so

%description devel
PdCom shall provide a flexible C++ API for non-realtime exchange of process
data and is part of the EtherLab project (http://etherlab.org/en/pdcom). It is
designed to be independent of any communication protocols and channels, and
shall be usable under several operating systems and platforms. I. e. it has
been tested successfully under Linux and Windows.

%files devel
%defattr(-,root,root)
%{_libdir}/lib%{pkgname}.so
%{_libdir}/pkgconfig/lib%{pkgname}.pc
%{_includedir}/%{pkgname}.h
%{_includedir}/%{pkgname}

#----------------------------------------------------------------------------
# Python library
#----------------------------------------------------------------------------
%define pythonlib python-pdcom
%define pyversion @PYLIB_MAJOR@.@PYLIB_MINOR@
%package -n %{pythonlib}

Summary:        Process data communication python client library
Version:        %pyversion
Group:          Development/Languages/Python
Obsoletes:      %{pythonlib} < %{pyversion}

%description -n %{pythonlib}
This package provides a Python wrapper for PdCom. See the main package for
more information.

%files -n %{pythonlib}
%defattr(-,root,root)
%{python_sitearch}/*

#----------------------------------------------------------------------------
# Development documentation package
#----------------------------------------------------------------------------
%package doc
Summary:        Documentation for %{name}
Group:          System/Libraries
BuildArch:      noarch

%description doc
Doxygen-generated API documentation for %{name}

%files doc
%defattr(-,root,root)
%doc _top_builddir/doc/html/
%doc example/example.cpp

#----------------------------------------------------------------------------
# Scriptlets section
#----------------------------------------------------------------------------
%post    -n %libname -p /sbin/ldconfig
%postun  -n %libname -p /sbin/ldconfig

%changelog
* Sat Nov 19 2016 lerichi@gmx.net
- Updated for use with OBS
